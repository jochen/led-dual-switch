const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

var state = {
  currentPosition: 0,
  targetPosition: 0,
  isRunning: false,
};

wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    client.send(data);
  });
};

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    const messagedecoded = JSON.parse(message);
    switch (messagedecoded.type) {
      case "setPosition":
        state.targetPosition = messagedecoded.value;
        if (!state.isRunning) {
          state.isRunning = true;
          setTimeout(updatePosition, 1000);
          setTimeout(sendUpdate, 200);
        }
        /*wss.broadcast(JSON.stringify({
          type: "position",
          value: messagedecoded.value,
        }));*/
        break;
      case "getPosition":
        ws.send(JSON.stringify({
          type: "position",
          value: state.currentPosition,
        }));
        /*wss.broadcast(JSON.stringify({
        type: "position",
        value: messagedecoded.value,
      }));*/
        break;
      case "buttonStop":
        wss.broadcast(JSON.stringify({
          type: "buttonAck",
          value: 1,
        }));
        break;
    }
  });

  //ws.send('something');
});

function updatePosition() {
  console.log("updatePosition", state);
  if (state.currentPosition > state.targetPosition) {
    state.currentPosition--;
  }
  if (state.currentPosition < state.targetPosition) {
    state.currentPosition++;
  }
  if (state.currentPosition != state.targetPosition) {
    setTimeout(updatePosition, 250);
  }
}

function sendUpdate() {
  if (state.isRunning) {
    console.log("updatePosition", state);
    wss.broadcast(JSON.stringify({
      type: "position",
      value: state.currentPosition,
    }));
  }
  if (state.currentPosition == state.targetPosition) {
    state.isRunning = false;
    wss.broadcast(JSON.stringify({
      type: "positionFinished",
      value: state.currentPosition,
    }));

  }
  else {
  setTimeout(sendUpdate, 200);
  }
}