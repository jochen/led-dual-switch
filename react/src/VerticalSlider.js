import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/lab/Slider';

const styles = {
  root: {
    display: 'flex',
    height: 300,
  },
  slider: {
    padding: '0px 22px',
  },
};

class VerticalSlider extends React.Component {
  constructor(props) {
    super(props);
    this.wsHandler = this.wsHandler.bind(this);
    this.state = {
      value: 50,
    };

  }

  handleChange = (event, value) => {
    this.setState({ value });
    this.props.socket.send(JSON.stringify({
      type: "setPosition",
      value,
    }))
  };

  wsHandler(ev) {
    console.log(ev.data);
    try {
      const decodeddata = JSON.parse(ev.data);
      switch (decodeddata.type) {
        case "position":
          this.setState({
            value: decodeddata.value
          })
          break;

      }
    } catch (e) {
      console.log(e, ev.data);
    }
  }
  componentDidMount() {
    this.props.socket.addEventListener("message", this.wsHandler);
  }

  componentWillUnmount() {
    this.props.socket.removeEventListener("message", this.wsHandler);
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <Slider
          classes={{ container: classes.slider }}
          value={value} step={1}
          onChange={this.handleChange}
          vertical
        />
      </div>
    );
  }
}

VerticalSlider.propTypes = {
  classes: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(VerticalSlider);