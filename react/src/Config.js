import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles, withTheme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import grey from '@material-ui/core/colors/grey';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import OnlineIndicator from './OnlineIndicator';
import Fab from '@material-ui/core/Fab';
import LoopIcon from '@material-ui/icons/Loop'
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import { Tooltip } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';



//import RaisedButton from 'material-ui/RaisedButton';

const styles = theme => ({
    /*
    root: {
        display: 'inline-block',
        //width: "50%",
        height: '100vh',
        //marginTop: 1,
        background: grey[100],
    },
    container: {
        display: 'flex',
        //display: 'center',
        flexWrap: 'wrap',
    },
    */
    root: {
        height: '100%',
        marginTop: 10,
        flexGrow: 1,

        //minHeight: '100vh',
        //flexGrow: 1,
        //display: 'flex',
        //alignItems: 'center',
    },

    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        maxWidth: 500,
    },

    textField: {
        //margin: 20,
        //width: '90%',
        //marginLeft: theme.spacing.unit,
        //marginRight: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
    fab: {
        margin: theme.spacing.unit,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },


});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class Config extends Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        this.sendForm = this.sendForm.bind(this);

        //console.log(JSON.stringify(this));
    }
    state = {
        mqtthost: '',
        mqttport: '1883',
        mqtttopicprefix: '',
        maxtemperature: '',
        hostname: '',
        realname: '',
        tosend: true,
        locked: true,
        getconfig: true,
        warnDialogOpen: false,
    };

    handleClickWarnDialogOpen = () => {
        this.setState({ warnDialogOpen: true });
    };

    handleWarnDialogClose = result => event => {
        this.setState({ warnDialogOpen: false });
        if (result) {
            console.log(result, event);
            this.props.socket.send(JSON.stringify({
                trigger: "wifireset",
            }));
        }
    };

    handleTriggerClick = message => event => {
        console.log(message, event);
        this.props.socket.send(JSON.stringify({
            trigger: message,
        }));

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
            tosend: false,
        });
        console.log(this.state);
        clearTimeout(this.timer);
        event.persist();
        this.timer = setTimeout(() => {
            this.sendForm(event);
        }, 2000);

    };

    sendForm = event => {
        clearTimeout(this.timer);
        console.log("sendform");
        console.log(event);
        event.preventDefault();
        console.log(this.state);
        this.props.socket.send(JSON.stringify({
            config: {
                mqtthost: this.state.mqtthost,
                mqtttopicprefix: this.state.mqtttopicprefix,
                maxtemperature: this.state.maxtemperature,
                hostname: this.state.hostname,
                realname: this.state.realname,
            }
        }));
        this.setState({
            tosend: true,
        });

    }
    wsHandler(ev) {
        //console.log(this.constructor.name, ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    if (this.state.getconfig) {
                        this.props.socket.send(JSON.stringify({
                            get: "config",
                        }));
                        this.setState({ getconfig: false })
                    }
                    break;

                case "message":
                    console.log(this.constructor.name, ev);
                    //console.log(ev.data);
                    const decodeddata = JSON.parse(ev.data);
                    switch (decodeddata.type) {
                        case "config":
                            console.log(decodeddata.config);
                            this.setState({
                                mqtthost: decodeddata.config.mqtthost,
                                mqtttopicprefix: decodeddata.config.mqtttopicprefix,
                                maxtemperature: decodeddata.config.maxtemperature,
                                hostname: decodeddata.config.hostname,
                                realname: decodeddata.config.realname,
                                locked: false,
                            });

                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }

    componentDidMount() {
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("open", this.wsHandler);
        if (this.state.getconfig) {
            this.props.socket.send(JSON.stringify({
                get: "config",
            }));
            this.setState({ getconfig: false })
        }

    }

    componentWillUnmount() {
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("open", this.wsHandler);
    }

    render() {
        const {
            tosend,
            locked,
            mqtthost,
            mqttport,
            mqtttopicprefix,
            maxtemperature,
            hostname,
            realname,
        } = this.state;

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={1}>
                    <form className={classes.container} noValidate autoComplete="off" onSubmit={this.sendForm}>
                        <Grid container spacing={24}>
                            <Grid item xs={7}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Mqtt Host or IP"
                                    placeholder="my.mqtt.host"
                                    className={classes.textField}
                                    onChange={this.handleChange('mqtthost')}
                                    helperText="Only resolvable Hostname or IP Address"
                                    value={mqtthost}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Mqtt Port"
                                    placeholder="1883"
                                    className={classes.textField}
                                    onChange={this.handleChange('mqttport')}
                                    helperText="Mqtt Server Port"
                                    value={mqttport}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid container item xs={2} alignItems="center" justify="center">
                                <Paper style={{ textAlign: 'center', height: 60 }}>
                                    <OnlineIndicator socket={this.props.socket} mode="message" color="gray" />
                                </Paper>
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Mqtt Topic Prefix"
                                    placeholder="top/sub1/sub2"
                                    className={classes.textField}
                                    onChange={this.handleChange('mqtttopicprefix')}
                                    helperText="Topic prefix without trailing slash"
                                    value={mqtttopicprefix}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Maximum Temperature"
                                    placeholder="100"
                                    className={classes.textField}
                                    onChange={this.handleChange('maxtemperature')}
                                    helperText="Set new switch off maximum Temperature"
                                    value={maxtemperature}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Hostname"
                                    placeholder="controller"
                                    className={classes.textField}
                                    onChange={this.handleChange('hostname')}
                                    helperText="Resolvable Hostname"
                                    value={hostname}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>
                            <Grid item xs={5}>
                                <TextField
                                    id="outlined-with-placeholder"
                                    label="Realname"
                                    placeholder="Controller"
                                    className={classes.textField}
                                    onChange={this.handleChange('realname')}
                                    helperText="Realname for identification"
                                    value={realname}
                                    fullWidth
                                    margin="normal"
                                    variant="outlined"
                                    disabled={locked}
                                />
                            </Grid>

                            <Grid item xs={2} justify="center" container alignItems="center">
                                <Button
                                    variant="contained"
                                    size="small"
                                    className={classes.button}
                                    type="submit"
                                    label="Save"
                                    primary="true"
                                    disabled={tosend}
                                >
                                    <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                                    Save
                                </Button>
                            </Grid>
                            <Grid item container xs={6} justify="center" alignItems="center">
                                <Typography variant="button" color="secondary" >
                                    Reset Wifi
                                </Typography>
                                <Tooltip title="Reset Wifi-Settings and restart">
                                <div>
                                    <Fab 
                                    color="secondary" 
                                    size="small" 
                                    aria-label="Delete" 
                                    className={classes.fab} 
                                    onClick={this.handleClickWarnDialogOpen}
                                    disabled={locked}
                                    >
                                        <DeleteIcon />
                                    </Fab>
                                    </div>
                                </Tooltip>

                                <Dialog
                                    open={this.state.warnDialogOpen}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleWarnDialogClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">
                                        {"Sure, you are resetting Wifi?"}
                                    </DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Resetting of Wifi brings the Device in AP Mode und must be configured.
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleWarnDialogClose(false)} color="primary">
                                            Cancel the Reset
                                        </Button>
                                        <Button onClick={this.handleWarnDialogClose(true)} color="primary">
                                            Yes, reset and restart.
                                        </Button>
                                    </DialogActions>
                                </Dialog>


                            </Grid>
                            <Grid item container xs={6} justify="center" alignItems="center">
                                <Typography variant="button" color="primary" >
                                    Restart
                                </Typography>
                                <Tooltip title="Restart (Reset) Controller">
                                <div>
                                    <Fab 
                                    color="primary" 
                                    size="small" 
                                    aria-label="Delete" 
                                    className={classes.fab} 
                                    onClick={this.handleTriggerClick("restart")}
                                    disabled={locked}
                                    >
                                        <LoopIcon />
                                    </Fab>
                                    </div>
                                </Tooltip>
                            </Grid>

                        </Grid>
                    </form>
                </Paper>
            </div>
        );
    }
}

Config.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Config);
