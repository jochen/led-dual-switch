import React, { Component } from 'react';
import ReconnectingWebSocket from 'reconnecting-websocket';
import Button from "@material-ui/core/Button";
import TemperatureGauge from './TemperatureGauge';
import ProgessButton from './ProgressButton';
import OnlineIndicator from './OnlineIndicator';
import './App.css';
import { connect } from 'net';
import HintMessage from './HintMessage';
import MenuAppBar from './MenuAppBar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Controller from './Controller';
import Config from './Config';
import { BrowserRouter as Router, Route, withRouter, Link } from 'react-router-dom';
import Debug from './Debug';


class App extends Component {

  constructor(props) {
    super(props);
    const searchParams = new URLSearchParams(window.location.search);
    //console.log(searchParams.get("s"));
    //this.handleClick = this.handleClick.bind(this);
    //var wshost = window.location.host;
    var wshost = searchParams.get("ws");
    if (wshost === null)
    {
      wshost = window.location.host;
    }
    this.state = {
      socket: new ReconnectingWebSocket('ws://' + wshost + '/control', 'optionalProtocol'),
      wshost,
    }
    //this.state.socket.addEventListener("message", this.ws2Handler);
    /*
    this.state.socket.addEventListener("open", this.wsHandler);
    this.state.socket.addEventListener("message", this.wsHandler);
    this.state.socket.addEventListener("close", this.wsHandler);
    this.state.socket.addEventListener("error", this.wsHandler);
    */
  }
  componentDidMount(){
    //console.log(props.location); // i can access the id here
  }
/*
  handleClick(value) {
    this.state.socket.send(JSON.stringify({
      type: "buttonStop",
      value: true,
    }));

  }
*/

  render() {
    //const { value } = this.state;

    return (
      <Router>
      <div className="App">
        <header className="App-header">
          <Route path="*" component={()=>(
            <MenuAppBar socket={this.state.socket} realName={this.state.wshost} />
        )}/>
          <Route exact path ="/" component={()=>(
              <Controller socket={this.state.socket} />
        )}/>
          <Route path="/config" component={()=>(
              <Config socket={this.state.socket} />
        )}/>
          <Route path="/debug" component={()=>(
              <Debug wshost={this.state.wshost}/>
        )}/>
        </header>
      </div>
      </Router>
      );
  }
}

export default App;
