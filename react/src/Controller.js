import React, { Component } from 'react';
import ReconnectingWebSocket from 'reconnecting-websocket';
import Button from "@material-ui/core/Button";
import TemperatureGauge from './TemperatureGauge';
import ProgessButton from './ProgressButton';
import SendValueButton from './SendValueButton';
import OnlineIndicator from './OnlineIndicator';
import { connect } from 'net';
import HintMessage from './HintMessage';
import MenuAppBar from './MenuAppBar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';


const styles = theme => ({
    root: {
        //minHeight: '100vh',
        marginTop: 10,
        flexGrow: 1,
        height: '100%',

        //display: 'flex',
        alignItems: 'center',
    },
    wrapper: {
        //margin: theme.spacing.unit,
        position: 'center',
    },
});


class Controller extends Component {
    constructor(props) {
        super(props);
        //console.log(JSON.stringify(this));
    }
/*    componentDidMount() {
        this.props.socket.send(JSON.stringify({
            type: "getPosition",
            value: -1,
        }))

    }
    handleClick = () => {
        this.props.socket.send(JSON.stringify({
            type: "led",
            value: 0,
        }));
    }
    ws2Handler(ev) {
        console.log("app ws", ev);

    }
*/
    render() {
        //const { value } = this.state;
        const { classes } = this.props;


        return (
            <div className={classes.root}>
                    <Grid container spacing={24} style={{ textAlign: "center" }}>
                        {/*
                        <Grid item xs={12}>
                            <OnlineIndicator socket={this.props.socket} />
                        </Grid>
                        */}
                        <Grid item xs={12}>
                            <TemperatureGauge socket={this.props.socket} label="Max Temp"></TemperatureGauge>
                        </Grid>
                        <Grid item xs={12}>
                            <SendValueButton socket={this.props.socket} color="secondary" value="1" typename="led">Led Top</SendValueButton>
                        </Grid>
                        <Grid item xs={12}>
                            <SendValueButton socket={this.props.socket} color="primary" value="0" typename="led">Off</SendValueButton>
                        </Grid>
                        <Grid item xs={12}>
                            <SendValueButton socket={this.props.socket} color="secondary" value="2" typename="led">Led Bottom</SendValueButton>
                        </Grid>
                        <Grid item xs={12}>
                            <HintMessage socket={this.props.socket} />
                        </Grid>
                    </Grid>
            </div>
        );
    }
}

Controller.propTypes = {
    classes: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(Controller);