import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },

    input: {
        color: 'white'
    },

    cssLabel: {
        color: 'lightblue',
    },

    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: `${theme.palette.primary.main} !important`,
        }
    },

    cssFocused: {},

    notchedOutline: {
        borderWidth: '1px',
        borderColor: 'lightblue !important',
    },

    cssHelperText: {
        color: 'yellow',
    },

});

class DebugTextField extends React.Component {
    /*  state = {
        name: 'InputMode',
      };
    
      handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
      };
    */
    render() {
        const { classes, key, label, helpertext, value } = this.props;

        return (
            <TextField
                key={key}
                id="standard-name"
                label={label}
                className={classes.textField}
                helperText={helpertext}
                value={value}
                margin="normal"
                variant="outlined"
                FormHelperTextProps={{
                    classes: {
                        root: classes.cssHelperText,
                    }
                }}
                InputLabelProps={{
                    classes: {
                        root: classes.cssLabel,
                        focused: classes.cssFocused,
                    },
                }}
                InputProps={{
                    classes: {
                        root: classes.cssOutlinedInput,
                        focused: classes.cssFocused,
                        notchedOutline: classes.notchedOutline,
                        input: classes.input,
                    },
                    //inputMode: "numeric"
                }}
            />
        );
    }
}

DebugTextField.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DebugTextField);
