import React from 'react';
import PropTypes, { func } from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

const styles1 = theme => ({
    close: {
        padding: theme.spacing.unit / 2,
    },
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
});

function HintSnackbarContent(props) {
    const { classes, className, message, onClose, variant } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            onClose={onClose}
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
        />
    );
}

HintSnackbarContent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const WrappedSnackbarContent = withStyles(styles1)(HintSnackbarContent);

const styles2 = theme => ({
    margin: {
        margin: theme.spacing.unit,
    },
});


class HintMessage extends React.Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        //console.log(JSON.stringify(this));
    }
    wsHandler(ev) {
        //console.log(ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    this.setState({
                        open: true,
                        message: "Connected",
                        variant: "info"
                    });
                    break;
                case "close":
                    this.setState({
                        open: true,
                        message: "Connection Lost",
                        variant: "error"
                    });
                    //console.log(decodeddata.value);
                    break;
                case "error":
                    this.setState({
                        open: true,
                        message: "Error",
                        variant: "error"
                    });
                    console.log("error", ev);
                    break;

                case "message":
                    console.log(this.constructor.name, ev);
                    //console.log(ev.data);
                    const decodeddata = JSON.parse(ev.data);
                    switch (decodeddata.type) {
                        case "position":
                            //console.log(decodeddata.value);
                            break;
                        case "positionFinished":
                            //console.log(decodeddata.value);
                            this.setState({
                                open: true,
                                message: "Position Arrived",
                                variant: "success"
                            });
                            break;
                        case "positionStopped":
                            //console.log(decodeddata.value);
                            this.setState({
                                open: true,
                                message: "Run stopped",
                                variant: "warning"
                            });
                            break;
                        default:
                            break;

                    }
                    break;
                default:
                    break;

            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }
    componentDidMount() {
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("open", this.wsHandler);
        this.props.socket.addEventListener("close", this.wsHandler);
        this.props.socket.addEventListener("error", this.wsHandler);
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("open", this.wsHandler);
        this.props.socket.removeEventListener("close", this.wsHandler);
        this.props.socket.removeEventListener("error", this.wsHandler);
    }
    state = {
        open: true,
        message: "Initialized",
        variant: "info",
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.open}
                    //classes={this.state.variant}
                    //className={"success"}
                    //variant="success"
                    autoHideDuration={6000}
                    onClose={this.handleClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
/*                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                  ]}
*/                >
                    <WrappedSnackbarContent
                        onClose={this.handleClose}
                        variant={this.state.variant}
                        message={this.state.message}
                    />
                </Snackbar>
            </div>
        );
    }
}

HintMessage.propTypes = {
    classes: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles2)(HintMessage);