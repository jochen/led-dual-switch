#!/bin/bash

targethost=$1

for todelete in $(curl -s http://$targethost/list?dir=/w | jq -r '.[].name'); do
  echo "delete: $todelete"
  curl -X DELETE "http://$targethost/upload?path=$todelete"
done

for datei in $(cd spiffs/; ls -1); do
  echo "upload: $datei"
  curl -v --form "file=@spiffs/${datei};filename=/w/${datei}" http://$targethost/upload


done

curl -s "http://$targethost/list?dir=/w" | jq
echo

