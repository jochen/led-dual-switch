#!/bin/bash

hostname=$1

curl  --progress-bar \
      -F 'name=update' \
      -F 'filename=@.pio/build/esp12e/firmware.bin'  \
      "http://$hostname/update" \
      | tee /dev/null


