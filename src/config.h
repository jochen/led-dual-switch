#define CONFIG_CHAR_LENGTH 40

#define LED_BUILTIN 2

#define LED_TOP_PIN 12
#define LED_BOTTOM_PIN 13

#define ONE_WIRE_BUS 4

extern const char *version;
extern const char *buildtime;

struct hostConfig
{
  char hostName[CONFIG_CHAR_LENGTH];
  char realName[CONFIG_CHAR_LENGTH];
  unsigned long crc = 0;
};

struct appConfig {
  char mqttHost[CONFIG_CHAR_LENGTH];
  char mqttTopicPrefix[CONFIG_CHAR_LENGTH];
  int maxTemperature = 100;
  unsigned long crc = 0;
};

struct logicDebugPin {
  const char *name;
  uint8_t pin;
};
