#include <Arduino.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define DEBUGOUT // Comment to disable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf_P(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

#include "config.h"

extern appConfig myappConfig;

extern void sendUdpSyslog(String msgtosend);
extern void broadcastWS(String type, int value);
extern void sendDebugWS(String name, String type, String value);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
unsigned long leddualloopmillis = 0;

unsigned long lastDebug = 0;
#define DEBUG_MIN_DELAY_MS 200
float lastTemperature = 0;

float getTemperatur() {
  float temp;
// int timeout;

// Serial.println("Messe ...");
// timeout=30;
// do {
 DS18B20.requestTemperatures(); 
 temp = DS18B20.getTempCByIndex(0);
// delay(100);
/* timeout--;
 if (timeout<0) temp=99.9; //Wenn Sensor defekt
 } while (temp == 85.0 || temp == -127.0) ;
 */
 return temp;
}

int temperature(int value) {
  return (int) getTemperatur() * 10;
}

int maxtemperature(int value) {
  return (int) myappConfig.maxTemperature;
}

// aus setup()
void setupLeddual()
{
  pinMode(ONE_WIRE_BUS, INPUT_PULLUP);

  pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED_BUILTIN pin as an output

  pinMode(LED_TOP_PIN, OUTPUT);

  pinMode(LED_BOTTOM_PIN, OUTPUT);

}

int led(int value)
{
  DEBUGLOG(PSTR("led: %u\n"), value);
  switch (value)
  {
  case 1:
    digitalWrite(LED_TOP_PIN, 1);
    digitalWrite(LED_BOTTOM_PIN, 0);
    break;
  case 2:
    digitalWrite(LED_TOP_PIN, 0);
    digitalWrite(LED_BOTTOM_PIN, 1);
    break;
  
  default:
    digitalWrite(LED_TOP_PIN, 0);
    digitalWrite(LED_BOTTOM_PIN, 0);
    break;
  }
  broadcastWS("led", value);
  return value;
}

int ledtoggle(int value)
{
  DEBUGLOG(PSTR("led: %u\n"), value);
  switch (value)
  {
  case 1:
    digitalWrite(LED_TOP_PIN, !digitalRead(LED_TOP_PIN));
    digitalWrite(LED_BOTTOM_PIN, 0);
    break;
  case 2:
    digitalWrite(LED_TOP_PIN, 0);
    digitalWrite(LED_BOTTOM_PIN, !digitalRead(LED_BOTTOM_PIN));
    break;
  
  default:
    Serial.println(F("Do nothing"));
    break;
  }
  broadcastWS("led", value);
  return value;
}


int ledtop(int value)
{
  DEBUGLOG(PSTR("ledtop: %u\n"), value);
  digitalWrite(LED_TOP_PIN, (bool) value);
  broadcastWS("ledtop", value);
  return -1;
}

int ledbottom(int value)
{
  DEBUGLOG(PSTR("ledbottom: %u\n"), value);
  digitalWrite(LED_BOTTOM_PIN, (bool) value);
  broadcastWS("ledbottom", value);
  return -1;
}


void loopLeddual()
{
    if ((leddualloopmillis + 1000) < millis())
    {
      leddualloopmillis = millis();
    //if ((millis() % 1000) == 0)
    //{
      float nowTemperatur = getTemperatur();
      if (nowTemperatur >= myappConfig.maxTemperature)
      {  
        led(0);
      }
      if (nowTemperatur != lastTemperature)
      {
          broadcastWS("temperature", (int) nowTemperatur * 10);
          lastTemperature = nowTemperatur;
      }

      sendDebugWS("Temperatur", "text", String(nowTemperatur));
    }

    //sendDebugWS("HeapFrag", "text", String(frag));

//        sendUdpSyslog((String("Rollo Ueberlast Notstop - mehr als 104W: ") + String(Watt)).c_str());
}
