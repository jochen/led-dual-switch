#include <Arduino.h>
#include <ArduinoJson.h>
#include <limits.h>
#include "FS.h"
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>
#include <Hash.h>
#include <functional>
#include <map>
#include <string>

#include "config.h"
#include "debugconfig.h"

extern AsyncWebServer server;
extern appConfig myappConfig;
extern hostConfig myhostConfig;

AsyncWebSocket wsdebug("/debug");
AsyncWebSocketClient *wsDebugClient = NULL;

void sendDebugWS(String name, String type, String value)
{
    if (wsDebugClient != NULL && wsDebugClient->status() == WS_CONNECTED)
    {
        DynamicJsonDocument doc(300);

        //StaticJsonDocument<200> doc;
        doc["name"] = name;
        doc["type"] = type;
        doc["millis"] = millis();
        doc["value"] = value;

        String config_serial;
        serializeJson(doc, config_serial);
        //client->text(config_serial);

        wsDebugClient->text(config_serial.c_str());
    }
}

void wsDebugEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
    Serial.println(F("wsEvent"));

    //Handle WebSocket event
    if (type == WS_EVT_CONNECT)
    {
        //client connected
        Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
        //client->printf("Hello Client %u :)", client->id());
        client->ping();
        /*
               myhostConfig.realName, \
           bufsz, adc0, \
           adcmin, adcmax, \
           String(AmpsRMS, 4).c_str(), String(Watt).c_str(), \
           String(lastCompleteRuntime).c_str(), String(lastruncomplete).c_str(), \
           String(percentOpen).c_str(), String(Z).c_str(), \
           String(lastpercentOpen).c_str(), \
           String(millis() / 1000).c_str() \
          );
        */
        DynamicJsonDocument doc(300);

        //StaticJsonDocument<100> doc;
        String config_serial;

/*         for (uint8_t logic = 0; logic < LOGICDEBUGCOUNT; logic++)
        {
            Serial.printf("logic %d\n", logic);
            Serial.printf("logicDebug %s - %d\n", logicDebug[logic].name, logicDebug[logic].pin);

            doc["name"] = logicDebug[logic].name;
            doc["type"] = "logic";
            doc["millis"] = millis();
            doc["value"] = digitalRead(logicDebug[logic].pin);

            config_serial = "";
            serializeJson(doc, config_serial);
            client->text(config_serial);
        } */

        doc["name"] = "Realname";
        doc["type"] = "text";
        doc["millis"] = millis();
        doc["value"] = myhostConfig.realName;

        config_serial = "";
        serializeJson(doc, config_serial);
        client->text(config_serial);

        //sendDebugWS("HeapFrag", "text", ESP.getResetInfo();
        doc["name"] = "ResetInfo";
        doc["type"] = "text";
        doc["millis"] = millis();
        doc["value"] = ESP.getResetInfo();

        config_serial = "";
        serializeJson(doc, config_serial);
        client->text(config_serial);

        doc["name"] = "ResetReason";
        doc["type"] = "text";
        doc["millis"] = millis();
        doc["value"] = ESP.getResetReason();

        config_serial = "";
        serializeJson(doc, config_serial);
        client->text(config_serial);

        doc["name"] = "Version";
        doc["type"] = "text";
        doc["millis"] = millis();
        doc["value"] = version;

        config_serial = "";
        serializeJson(doc, config_serial);
        client->text(config_serial);

        doc["name"] = "Buildtime";
        doc["type"] = "text";
        doc["millis"] = millis();
        doc["value"] = buildtime;

        config_serial = "";
        serializeJson(doc, config_serial);
        client->text(config_serial);

        /*
        client->printf(R"V0G0N(
        {
                       myhostConfig.realName, \
           bufsz, adc0, \
           adcmin, adcmax, \
           String(AmpsRMS, 4).c_str(), String(Watt).c_str(), \
           String(lastCompleteRuntime).c_str(), String(lastruncomplete).c_str(), \
           String(percentOpen).c_str(), String(Z).c_str(), \
           String(lastpercentOpen).c_str(), \
           String(millis() / 1000).c_str() \
          );


        }
)V0G0N";
*/
        wsDebugClient = client;
    }
    else if (type == WS_EVT_DISCONNECT)
    {
        //client disconnected
        Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
        wsDebugClient = NULL;
    }
    else if (type == WS_EVT_ERROR)
    {
        //error was received from the other end
        Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t *)arg), (char *)data);
    }
    else if (type == WS_EVT_PONG)
    {
        //pong message was received (in response to a ping request maybe)
        Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len) ? (char *)data : "");
    }
    else if (type == WS_EVT_DATA)
    {
        //data packet
        AwsFrameInfo *info = (AwsFrameInfo *)arg;
        if (info->final && info->index == 0 && info->len == len)
        {
            //the whole message is in a single frame and we got all of it's data
            Serial.printf("ws[%s][%u] %s-message[%llu]: \n", server->url(), client->id(), (info->opcode == WS_TEXT) ? "text" : "binary", info->len);
            if (info->opcode == WS_TEXT)
            {
                {
                    char s_data[len + 1];
                    s_data[len] = '\0';
                    strncpy(s_data, (char *)data, len);
                    Serial.printf("s_data:%s\n", s_data);
                }
                DynamicJsonDocument root(len + 128);
                //JsonObject &root = jsonBuffer.parseObject((char *)jsonMessage);
                auto jsonerror = deserializeJson(root, data);
                if (!jsonerror)
                {
                    Serial.println("JSON parse success");
                    JsonObject rootobj = root.as<JsonObject>();
                    if (rootobj.containsKey("type") &&
                        rootobj.containsKey("value") &&
                        rootobj.containsKey("name"))
                    {
                        if (root["type"] == "logic")
                        {
                            for (uint8_t logic = 0; logic < LOGICDEBUGCOUNT; logic++)
                            {
                                if (root["name"] == String(logicDebug[logic].name))
                                {
                                    Serial.printf("get logicDebug %s - %d\n", logicDebug[logic].name, logicDebug[logic].pin);
                                    uint8_t value = root["value"];
                                    digitalWrite(logicDebug[logic].pin, value);

                                    DynamicJsonDocument doc(300);
                                    //StaticJsonDocument<200> doc;
                                    String config_serial;

                                    doc["name"] = logicDebug[logic].name;
                                    doc["type"] = "logic";
                                    doc["millis"] = millis();
                                    doc["value"] = digitalRead(logicDebug[logic].pin);

                                    serializeJson(doc, config_serial);
                                    client->text(config_serial);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for (size_t i = 0; i < info->len; i++)
                {
                    Serial.printf("%02x ", data[i]);
                }
                Serial.printf("\n");
            }
            /* // websocket debugging
      if (info->opcode == WS_TEXT)
        client->text("I got your text message");
      else
        client->binary("I got your binary message");
      */
        }
        else
        {
            //message is comprised of multiple frames or the frame is split into multiple packets
            if (info->index == 0)
            {
                if (info->num == 0)
                    Serial.printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
                Serial.printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(), client->id(), info->num, info->len);
            }

            Serial.printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT) ? "text" : "binary", info->index, info->index + len);
            if (info->message_opcode == WS_TEXT)
            {
                data[len] = 0;
                Serial.printf("%s\n", (char *)data);
            }
            else
            {
                for (size_t i = 0; i < len; i++)
                {
                    Serial.printf("%02x ", data[i]);
                }
                Serial.printf("\n");
            }

            if ((info->index + len) == info->len)
            {
                Serial.printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(), client->id(), info->num, info->len);
                if (info->final)
                {
                    Serial.printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
                    if (info->message_opcode == WS_TEXT)
                        client->text("I got your text message");
                    else
                        client->binary("I got your binary message");
                }
            }
        }
    }
}

void setupDebug()
{
    pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED_BUILTIN pin as an output

    wsdebug.onEvent(wsDebugEvent);
    server.addHandler(&wsdebug);
}

void loopDebug()
{
    if (wsDebugClient != NULL && (millis() % 2000) == 0)
    {
        long mem = ESP.getFreeHeap();
        sendDebugWS("FreeHeap", "text", String(mem));
        auto frag = ESP.getHeapFragmentation();
        sendDebugWS("HeapFrag", "text", String(frag));
        //Serial.println("loopDebug % 5000");

        auto rssi = WiFi.RSSI();
        sendDebugWS("RSSI", "text", String(rssi));

        for (uint8_t logic = 0; logic < LOGICDEBUGCOUNT; logic++)
        {
            sendDebugWS(String(logicDebug[logic].name), "logic", String(digitalRead(logicDebug[logic].pin)));
        }
    }
}
