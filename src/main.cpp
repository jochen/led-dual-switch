#include <Arduino.h>
#include <ArduinoJson.h>
#include <limits.h>
#include "FS.h"
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>
#include <Hash.h>
#include <functional>
#include <map>
#include <string>

#include "config.h"

#define DEBUGOUT // Comment to enable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

extern unsigned long crc(uint8_t *blob, unsigned int size);
extern void handleFileUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);
extern void handleFileDelete(AsyncWebServerRequest *request);
extern void updateFirmware(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);
extern void handleFileList(AsyncWebServerRequest *request);
extern void handleFileRead(AsyncWebServerRequest *request);

extern void sendDebugWS(String name, String type, String value);

#define DEFAULT_HOSTNAME "ledlight"
#define MDNSSERVICENAME DEFAULT_HOSTNAME

#define XSTR(x) STR(x)
#define STR(x) #x
#ifndef BUILDTIME
#define BUILDTIME "none"
#endif
#pragma message("BUILDTIME=" XSTR(BUILDTIME))
const char *version = "0.2";
const char *buildtime = (const char *)BUILDTIME;


//with starting slash
#define INTOPIC_SUFFIX "/ledlight/setpercent"
#define OUTTOPIC_SUFFIX "/ledlight/percent"

#define MAXTOPICLENGTH 128
AsyncMqttClient mqttClient;

DNSServer dns;

AsyncWebServer server(80);
AsyncWebSocket wscontrol("/control");
AsyncWiFiManager wifiManager(&server, &dns);

//flag to use from web update to reboot the ESP
bool shouldReboot = false;
bool mqttDoReconnect = false;
unsigned long noWifiRestartTimeout = 0;

unsigned long mainloopmillis = 0;

int lastPosition = -1;

appConfig myappConfig;
hostConfig myhostConfig;

extern void setupLeddual();
extern void loopLeddual();

extern void setupDebug();
extern void loopDebug();

//extern int buttonStop(int value);
//extern int setPosition(int value);
//extern int getPosition(int value);
extern int led(int value);
extern int ledtoggle(int value);
extern int temperature(int value);
extern int maxtemperature(int value);
extern int ledtop(int value);
extern int ledbottom(int value);

std::map<std::string, std::function<int(int)>> wsMap =
    {
        {"led", led},
        {"ledtoggle", ledtoggle},
        {"temperature", temperature},
        {"maxtemperature", maxtemperature},
        {"ledtop", ledtop},
        {"ledbottom", ledbottom}};

//typedef int (*FnPtr)(int);
//std::map<std::string, FnPtr> wsMap;
//wsMap["buttonStop"] = buttonStop;
//wsMap["position"] = position;

//DynamicJsonBuffer jsonBuffer;
//StaticJsonBuffer<128> jsonBuffer;

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback()
{
  Serial.println(F("Should save config\n"));
  shouldSaveConfig = true;
}

void onNotFound(AsyncWebServerRequest *request)
{
  //Handle Unknown Request
  request->send(404);
}

void broadcastWS(String type, int value)
{
  wscontrol.printfAll("{ \"type\": \"%s\", \"value\": %d }", type.c_str(), value);
}

void saveAppConfig()
{
  unsigned long newcrc = crc((uint8_t *)&myappConfig, sizeof(appConfig));
  if (newcrc != myappConfig.crc)
  {
    myappConfig.crc = newcrc;
    EEPROM.put(sizeof(hostConfig), myappConfig);
    EEPROM.commit();
    Serial.println(myappConfig.crc);
    mqttDoReconnect = true;
  }
  else
  {
    Serial.println("appConfig not changed");
  }
}

void connectToMqtt()
{
  if (!mqttClient.connected())
  {
    Serial.println("Connecting to MQTT...");
    mqttClient.setServer(myappConfig.mqttHost, 1883); //myappConfig.mqttPort);
    mqttClient.setMaxTopicLength(MAXTOPICLENGTH);
    mqttClient.connect();
  }
  else
  {
    Serial.println("MQTT already connected!");
  }
}

void saveHostConfig()
{
  Serial.println(F("Save new Hostconfig"));
  myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
  EEPROM.put(0, myhostConfig);
  EEPROM.commit();
  WiFi.hostname(myhostConfig.hostName);
  Serial.println(myhostConfig.hostName);
  Serial.println(myhostConfig.realName);
  Serial.println(myhostConfig.crc);
}

void wsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len)
{
  Serial.println(F("wsEvent"));
  noWifiRestartTimeout = millis();
  //Handle WebSocket event
  if (type == WS_EVT_CONNECT)
  {
    //client connected
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
    //client->printf("Hello Client %u :)", client->id());
    client->ping();
    DynamicJsonDocument doc(100);
    //StaticJsonDocument<100> doc;
    doc["type"] = "info";
    doc["realname"] = myhostConfig.realName;
    String to_send = "";
    serializeJson(doc, to_send);
    Serial.println(to_send);
    client->text(to_send);
    Serial.println("info sended");
  }
  else if (type == WS_EVT_DISCONNECT)
  {
    //client disconnected
    Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
  }
  else if (type == WS_EVT_ERROR)
  {
    //error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t *)arg), (char *)data);
  }
  else if (type == WS_EVT_PONG)
  {
    //pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len) ? (char *)data : "");
  }
  else if (type == WS_EVT_DATA)
  {
    //data packet
    AwsFrameInfo *info = (AwsFrameInfo *)arg;
    if (info->final && info->index == 0 && info->len == len)
    {
      //the whole message is in a single frame and we got all of it's data
      Serial.printf("ws[%s][%u] %s-message[%llu]: \n", server->url(), client->id(), (info->opcode == WS_TEXT) ? "text" : "binary", info->len);
      if (info->opcode == WS_TEXT)
      {
        {
          char s_data[len + 1];
          s_data[len] = '\0';
          strncpy(s_data, (char *)data, len);
          Serial.printf("s_data:%s\n", s_data);
        }

        DynamicJsonDocument root(len + 128);
        //JsonObject &root = jsonBuffer.parseObject((char *)jsonMessage);
        auto jsonerror = deserializeJson(root, data);
        if (!jsonerror)
        {
          Serial.println("JSON parse success");
          JsonObject rootobj = root.as<JsonObject>();
          if (rootobj.containsKey("type"))
          {
            const char *wstype = root["type"];
            Serial.printf("wstype: %s\n", wstype);
            if (wstype && wsMap.count(wstype) == 1)
            {
              int value = root["value"];
              int wsres = wsMap[wstype](value);
              Serial.printf("value: %i, wsres: %i\n", value, wsres);
              client->text(String(String("{ \"type\": \"") + wstype + "\", \"result\": " + String(wsres) + " }"));
            }
          }
          else if (rootobj.containsKey("config"))
          {
            Serial.println("Contains Key config");
            JsonObject config = rootobj["config"];

            if (config.containsKey("mqtthost") && 
              config.containsKey("mqtttopicprefix") &&
              config.containsKey("maxtemperature"))
            {
              strcpy(myappConfig.mqttHost, config["mqtthost"]);
              Serial.print("mqtthost:");
              Serial.println(myappConfig.mqttHost);
              strcpy(myappConfig.mqttTopicPrefix, config["mqtttopicprefix"]);
              Serial.print("mqtttopicprefix:");
              Serial.println(myappConfig.mqttTopicPrefix);
              myappConfig.maxTemperature = config["maxtemperature"];
              saveAppConfig();
              broadcastWS("maxtemperature", myappConfig.maxTemperature);
            }
            if (config.containsKey("hostname") &&
                config.containsKey("realname") &&
                config["hostname"] != "" &&
                config["realname"] != "")
            {
              strcpy(myhostConfig.hostName, config["hostname"]);
              Serial.print("hostname:");
              Serial.println(myhostConfig.hostName);
              strcpy(myhostConfig.realName, config["realname"]);
              Serial.print("realname:");
              Serial.println(myhostConfig.realName);
              saveHostConfig();
            }
            //mqttClient.disconnect();
            //connectToMqtt();
          }
          else if (rootobj.containsKey("get"))
          {
            if (rootobj["get"] == String("config"))
            {
              Serial.println("get config");
              String config_serial;

              DynamicJsonDocument doc(300);
              //StaticJsonDocument<200> doc;
              doc["type"] = "config";
              JsonObject config = doc.createNestedObject("config");
              config["mqtthost"] = myappConfig.mqttHost;
              config["mqtttopicprefix"] = myappConfig.mqttTopicPrefix;
              config["hostname"] = myhostConfig.hostName;
              config["realname"] = myhostConfig.realName;
              config["maxtemperature"] = myappConfig.maxTemperature;

              config_serial = "";
              serializeJson(doc, config_serial);
              Serial.println(config_serial);
              client->text(config_serial);
              Serial.println("config_serial sended");
              broadcastWS("mqttconnection", (int)mqttClient.connected());
              Serial.println("mqttconnection sended");
            }
          }
          else if (rootobj.containsKey("trigger"))
          {
            if (rootobj["trigger"] == String("wifireset"))
            {
              wifiManager.resetSettings();
              shouldReboot = true;
            }
            else if (rootobj["trigger"] == String("restart"))
            {
              shouldReboot = true;
            }
          }
        }
      }
      else
      {
        for (size_t i = 0; i < info->len; i++)
        {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }
      /* // websocket debugging
      if (info->opcode == WS_TEXT)
        client->text("I got your text message");
      else
        client->binary("I got your binary message");
      */
    }
    else
    {
      //message is comprised of multiple frames or the frame is split into multiple packets
      if (info->index == 0)
      {
        if (info->num == 0)
          Serial.printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
        Serial.printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(), client->id(), info->num, info->len);
      }

      Serial.printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT) ? "text" : "binary", info->index, info->index + len);
      if (info->message_opcode == WS_TEXT)
      {
        data[len] = 0;
        Serial.printf("%s\n", (char *)data);
      }
      else
      {
        for (size_t i = 0; i < len; i++)
        {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }

      if ((info->index + len) == info->len)
      {
        Serial.printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(), client->id(), info->num, info->len);
        if (info->final)
        {
          Serial.printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
          if (info->message_opcode == WS_TEXT)
            client->text("I got your text message");
          else
            client->binary("I got your binary message");
        }
      }
    }
  }
  Serial.println("ws event end");

}

void addFileToWWW(String path)
{
  Serial.printf("Add File to WWW: %s\n", path.substring(2).c_str());
  server.on(path.substring(2).c_str(), HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
}
void delFileFromWWW(String path)
{
  Serial.printf("Del File from WWW: %s\n", path.substring(2).c_str());
  auto delhandler = server.on(path.substring(2).c_str(), [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
  //don't need handler anymore remove it
  server.removeHandler(&delhandler);
}

void scanWWWDir()
{
  Dir dir = SPIFFS.openDir("/w");
  while (dir.next())
  {
    String fileName = dir.fileName();
    addFileToWWW(fileName);
    /*
    server.on(fileName.substring(2).c_str(), HTTP_GET, [](AsyncWebServerRequest *request) {
      handleFileRead(request);
    });
    */
  }
}

/*
void mqttPublishPosition()
{
  String topicPrefix = String(myappConfig.mqttTopicPrefix);
  int position = getPosition(-1);

  uint16_t packetIdPub = mqttClient.publish(
      String(topicPrefix + "/" + myhostConfig.hostName + OUTTOPIC_SUFFIX).c_str(), 1, true,
      String(position).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub);
}
*/

void onMqttConnect(bool sessionPresent)
{
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  String topicPrefix = String(myappConfig.mqttTopicPrefix);

  Serial.printf("Subscribe Topic: %s\n", String(topicPrefix + "/" + myhostConfig.hostName + INTOPIC_SUFFIX).c_str());
  sendDebugWS("Sub-Topic", "text", String(topicPrefix + "/" + myhostConfig.hostName + INTOPIC_SUFFIX));
  uint16_t packetIdSub = mqttClient.subscribe(
      String(topicPrefix + "/" + myhostConfig.hostName + INTOPIC_SUFFIX).c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub);

  //mqttPublishPosition();
  broadcastWS("mqttconnection", 1);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  Serial.println("Disconnected from MQTT.");
  /*
  TCP_DISCONNECTED = 0,
  MQTT_UNACCEPTABLE_PROTOCOL_VERSION = 1,
  MQTT_IDENTIFIER_REJECTED = 2,
  MQTT_SERVER_UNAVAILABLE = 3,
  MQTT_MALFORMED_CREDENTIALS = 4,
  MQTT_NOT_AUTHORIZED = 5,
  ESP8266_NOT_ENOUGH_SPACE = 6,
  TLS_BAD_FINGERPRINT = 7
  */
  switch (reason)
  {
    case AsyncMqttClientDisconnectReason::TCP_DISCONNECTED:
      Serial.println("TCP_DISCONNECTED");
      break;
    case AsyncMqttClientDisconnectReason::MQTT_UNACCEPTABLE_PROTOCOL_VERSION:
      Serial.println("MQTT_UNACCEPTABLE_PROTOCOL_VERSION");
      break;
    case AsyncMqttClientDisconnectReason::MQTT_IDENTIFIER_REJECTED:
      Serial.println("MQTT_IDENTIFIER_REJECTED");
      break;
    case AsyncMqttClientDisconnectReason::MQTT_SERVER_UNAVAILABLE:
      Serial.println("MQTT_SERVER_UNAVAILABLE");
      break;
    case AsyncMqttClientDisconnectReason::MQTT_MALFORMED_CREDENTIALS:
      Serial.println("MQTT_MALFORMED_CREDENTIALS");
      break;
    case AsyncMqttClientDisconnectReason::MQTT_NOT_AUTHORIZED:
      Serial.println("MQTT_NOT_AUTHORIZED");
      break;
    case AsyncMqttClientDisconnectReason::ESP8266_NOT_ENOUGH_SPACE:
      Serial.println("ESP8266_NOT_ENOUGH_SPACE");
      break;
    case AsyncMqttClientDisconnectReason::TLS_BAD_FINGERPRINT:
      Serial.println("TLS_BAD_FINGERPRINT");
      break;
  
    default:
      Serial.println("UNKNOWN REASON");
      break;
  }

  broadcastWS("mqttconnection", 0);
}

void onMqttPublish(uint16_t packetId)
{
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId)
{
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
  Serial.print("Publish received, ");
  Serial.print("  topic: ");
  Serial.print(topic);
  Serial.print(",  qos: ");
  Serial.print(properties.qos);
  Serial.print(",  dup: ");
  Serial.print(properties.dup);
  Serial.print(",  retain: ");
  Serial.print(properties.retain);
  Serial.print(",  len: ");
  Serial.print(len);
  Serial.print(",  index: ");
  Serial.print(index);
  Serial.print(",  total: ");
  Serial.print(total);
  if (len < 20)
  {
    Serial.print("  payload: ");

    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    Serial.println(s_payload);
  }
  else
  {
    Serial.println("  payload len>60");
    /*
    char s_payload[len+1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);
    */
  }

  String topicPrefix = String(myappConfig.mqttTopicPrefix);

  if (String(topic) == String(topicPrefix + "/" + myhostConfig.hostName + INTOPIC_SUFFIX) &&
      total > 0 &&
      total == len &&
      index == 0)
  {
    char s_payload[len + 1];
    s_payload[len] = '\0';
    strncpy(s_payload, payload, len);

    String strPayload = String((char *)s_payload);
    int percent = strPayload.toInt();
    Serial.println(percent);
    if (percent == 0)
    {
      led(0);
    }
    else if (percent == 1)
    {
      led(1);
    }
    else if (percent >= 2)
    {
      led(2);
    }
    
  }
}

void handleRestCall(AsyncWebServerRequest *request)
{
    String argfunc;
    String argvalue;
    if (request->hasArg("func"))
    {
        argfunc = request->arg("func");
    }
    else
    {
        argvalue = String("");
    }
    if (request->hasArg("value"))
    {
        argvalue = request->arg("value");
    }
    else
    {
        argvalue = String("");
    }

    DEBUGLOG("handleRestCall: %s => %s\r\n", argfunc.c_str(), argvalue.c_str());

    const char *wstype = argfunc.c_str();
    if (wstype && wsMap.count(wstype) == 1)
    {
      int value = argvalue.toInt();
      int wsres = wsMap[wstype](value);
      Serial.printf("value: %i, wsres: %i\n", value, wsres);
      request->send(200, "text/json", String(String("{ \"func\": \"") + wstype + "\", \"result\": " + String(wsres) + " }"));
    }
    else{
      request->send(404, "text/plain", "Function Not Found");
    }
}


bool setupWifi(bool force)
{
  bool success = false;
  char newHostname[40] = DEFAULT_HOSTNAME;
  char newRealname[40] = DEFAULT_HOSTNAME;

  wifiManager.setConfigPortalTimeout(300);
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  if (force)
  {
    wifiManager.resetSettings();
  }

  if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
  {
    Serial.println(F("Valid Wifi Config readed"));
    Serial.println(myhostConfig.hostName);
    Serial.println(myhostConfig.realName);
    Serial.println(myhostConfig.crc);
    WiFi.hostname(myhostConfig.hostName);
    strcpy(newHostname, myhostConfig.hostName);
    strcpy(newRealname, myhostConfig.realName);
    //wifiManager.setHostname(myhostConfig.hostName);
    //success = wifiManager.autoConnect(myhostConfig.hostName);
    //ArduinoOTA.setHostname(myhostConfig.hostName);
  }
  else
  {
    Serial.println(F("Invalid Wifi Config readed"));
    Serial.println(myhostConfig.hostName);
    Serial.println(myhostConfig.realName);
    Serial.println(myhostConfig.crc);
    wifiManager.resetSettings();
    strcpy(myhostConfig.hostName, DEFAULT_HOSTNAME);
    strcpy(myhostConfig.realName, DEFAULT_HOSTNAME);
    //success = wifiManager.autoConnect(myhostConfig.hostName);
    myhostConfig.crc = 0;
  }

  AsyncWiFiManagerParameter setNewHostname("Hostname", "LED Light Hostname", newHostname, 40);
  AsyncWiFiManagerParameter setNewRealname("Realname", "LED Light Realname", newRealname, 40);
  wifiManager.addParameter(&setNewHostname);
  wifiManager.addParameter(&setNewRealname);
  success = wifiManager.autoConnect(myhostConfig.hostName);

  if (shouldSaveConfig && String(setNewHostname.getValue()) != "" && String(setNewRealname.getValue()) != "")
  {
    strcpy(myhostConfig.hostName, setNewHostname.getValue());
    strcpy(myhostConfig.realName, setNewRealname.getValue());
    saveHostConfig();
  }
  return success;
}

void setup()
{

  Serial.begin(74880);
  Serial.println(F("Booting"));
  EEPROM.begin(sizeof(hostConfig) + sizeof(appConfig));

  Serial.print(F("getSketchSize: "));
  Serial.println(ESP.getSketchSize());
  Serial.print(F("getResetReason:"));
  Serial.println(ESP.getResetReason());
  Serial.print(F("getResetInfo: "));
  Serial.println(ESP.getResetInfo());
  Serial.print(F("getCycleCount: "));
  Serial.println(ESP.getCycleCount());

  EEPROM.get(0, myhostConfig);
  EEPROM.get(sizeof(hostConfig), myappConfig);

  if (myappConfig.crc == crc((uint8_t *)&myappConfig, sizeof(appConfig)))
  {
    Serial.println(F("Valid APP Config readed"));
    Serial.println(myappConfig.crc);
  }
  else
  {
    Serial.println(F("No Valid APP Config"));
    myappConfig.crc = 0;
    myappConfig.mqttHost[0] = '\0';
    myappConfig.mqttTopicPrefix[0] = '\0';
  }
  if (setupWifi(false))
  {
    Serial.println(F("Wifi setup successful"));
  }
  Serial.println(F("Ready"));
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());

  if (MDNS.begin(myhostConfig.hostName))
  {
    Serial.println(F("MDNS responder started"));
    MDNS.addService("http", "tcp", 80);
    MDNS.addService(MDNSSERVICENAME, "tcp", 80);
    Serial.println(F("MDNS services added"));
  }

  SPIFFS.begin();
  {
    Serial.println("SPIFFS contents:");

    Dir dir = SPIFFS.openDir("/");
    while (dir.next())
    {
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      Serial.printf("FS File: %s, size: %s\n", fileName.c_str(), String(fileSize).c_str());
    }
    Serial.printf("\n");
  }
  server.reset();
  //w as www
  //server.serveStatic("/", SPIFFS, "/w/").setDefaultFile("index.html");

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileRead(request);
  });
  scanWWWDir();

  // attach filesystem root at URL /fs
  server.serveStatic("/fs", SPIFFS, "/");
  server.on("/list", HTTP_GET, [](AsyncWebServerRequest *request) {
    handleFileList(request);
  });

  server.on("/rest", HTTP_GET, [](AsyncWebServerRequest *request) {
    handleRestCall(request);
  });

  // attach AsyncWebSocket
  wscontrol.onEvent(wsEvent);
  server.addHandler(&wscontrol);

  server.onNotFound(onNotFound);

  // Simple Firmware Update Form
  server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/html", "<form method='POST' action='/update' enctype='multipart/form-data'><input type='file' name='update'><input type='submit' value='Update'></form>");
  });

  server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request) {
    shouldReboot = !Update.hasError();
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", shouldReboot?"OK":"FAIL");
    response->addHeader("Connection", "close");
    request->send(response); }, [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) { updateFirmware(request, filename, index, data, len, final); });

  //upload a file to /upload
  server.on("/upload", HTTP_POST, [](AsyncWebServerRequest *request) { request->send(200); },
            [](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final) {
              handleFileUpload(request, filename, index, data, len, final);
            });

  server.on("/upload", HTTP_DELETE, [](AsyncWebServerRequest *request) {
    handleFileDelete(request);
  });

  server.begin();
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);

  setupLeddual();
  setupDebug();
}

void loop()
{
  MDNS.update();
  if (shouldReboot)
  {
    Serial.println("Rebooting...");
    delay(100);
    ESP.restart();
  }
  if (!WiFi.isConnected() && millis() > (noWifiRestartTimeout + 1800000))
  {
    Serial.println("no Wifi Timeout, rebooting...");
    delay(100);
    ESP.restart();
  }

  //has valid appConfig
  if (WiFi.isConnected() && myappConfig.crc != 0)
  {
    if (String(myappConfig.mqttHost) != "" && String(myappConfig.mqttTopicPrefix) != "")
      if (!mqttClient.connected() && (millis() % 1000) == 0)
      {
        connectToMqtt();
      }
  }
  if (mqttDoReconnect)
  {
    if (mqttClient.connected())
    {
      Serial.println("Force mqtt disconnect");
      mqttClient.disconnect();
    }
    mqttDoReconnect = false;
  }

/*
  if (millis() % 200 == 0)
  {
    int position = getPosition(-1);
    if (lastPosition != position)
    {
      broadcastWS("position", position);
      mqttPublishPosition();
      lastPosition = position;
    }
  }
*/
  if ((mainloopmillis + 10000) < millis())
  {
    mainloopmillis = millis();
    Serial.print("Main loop: ");
    Serial.println(mainloopmillis);

  }

  loopLeddual();
  loopDebug();
}
