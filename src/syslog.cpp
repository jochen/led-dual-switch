#include <ESP8266WiFi.h>
//#include "ESPAsyncUDP.h"
#include <WiFiUdp.h>
#include "config.h"

extern struct hostConfig myhostConfig;

WiFiUDP syslogudp;

void sendUdpSyslog(String msgtosend)
{
  
  unsigned int msg_length = msgtosend.length();
  byte* p = (byte*)malloc(msg_length);
  memcpy(p, (char*) msgtosend.c_str(), msg_length);

  //syslogudp.beginPacket(syslogServer, 514);
  syslogudp.beginPacket(WiFi.gatewayIP(), 514);
  syslogudp.write(myhostConfig.hostName);
  syslogudp.write(" ");
  syslogudp.write(p, msg_length);
  syslogudp.endPacket();
  free(p);
  
}
